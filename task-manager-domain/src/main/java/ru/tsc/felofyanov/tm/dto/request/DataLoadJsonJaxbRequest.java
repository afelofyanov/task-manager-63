package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadJsonJaxbRequest extends AbstractUserRequest {

    public DataLoadJsonJaxbRequest(@Nullable String token) {
        super(token);
    }
}
