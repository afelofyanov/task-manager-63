package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {
    
    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {return INSTANCE;}
    
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Task1"));
        add(new Task("Task2"));
        add(new Task("Task3"));
    }
    
    public void create() {
        add(new Task("New Task: " + System.currentTimeMillis()));
    } 
    
    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }
    
    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }
    
    public Collection<Task> findAll() {
        return tasks.values();
    }
    
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }
    
    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
