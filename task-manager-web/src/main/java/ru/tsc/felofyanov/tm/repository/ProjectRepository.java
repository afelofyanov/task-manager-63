package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {return INSTANCE;}

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("Test"));
        add(new Project("One"));
        add(new Project("Two"));
    }

    public void create() {
        add(new Project("New project: " + System.currentTimeMillis()));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(String id) {
        projects.remove(id);
    }

}
